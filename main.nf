#!/usr/bin/env nextflow

process Test {
    publishDir 'results'

    output:
    file 'test.txt' into stuff

    """
    echo "Printing for test purposes" > test.txt
    """

}
